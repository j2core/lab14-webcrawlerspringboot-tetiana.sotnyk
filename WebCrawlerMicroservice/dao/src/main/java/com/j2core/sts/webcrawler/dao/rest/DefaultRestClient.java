package com.j2core.sts.webcrawler.dao.rest;

import com.j2core.sts.webcrawler.dao.model.dto.FinalInformationDto;
import com.j2core.sts.webcrawler.dao.model.dto.TaskResultDto;
import com.j2core.sts.webcrawler.dao.model.informationdto.NodeData;
import com.j2core.sts.webcrawler.dao.model.informationdto.UrlData;
import com.j2core.sts.webcrawler.dao.model.userdto.SecurityToken;
import com.j2core.sts.webcrawler.dao.model.userdto.UserData;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by sts on 8/15/17.
 */

/**
 *
 *The class is default rest client interface implementation
 */
public class DefaultRestClient implements RestClient{

    private final static Logger LOGGER = Logger.getLogger(DefaultRestClient.class); // class for save logs information
    private static final String REST_URL = "http://localhost:8080/TaskService/API";
    private Client client;


    public DefaultRestClient(){

        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, 6000);
        configuration.property(ClientProperties.READ_TIMEOUT, 6000);

        client = ClientBuilder.newClient(configuration);

    }


    @Override
    public SecurityToken getSecurityToken(UserData user){

        SecurityToken token = null;

        try {

            token = client.target(REST_URL).path("auth/get").request(MediaType.APPLICATION_JSON).
                    post(Entity.entity(user, MediaType.APPLICATION_JSON)).readEntity(SecurityToken.class);

        }catch (Exception ex){

            LOGGER.error(" getToken " + ex);
        }

        return token;

    }

    @Override
    public NodeData getNodeData(int nodeId, SecurityToken token) throws Exception {

        String path = "node/get/" + String.valueOf(nodeId);

        Response response = client.target(REST_URL).path(path).request(MediaType.APPLICATION_JSON).
                post(Entity.entity(token, MediaType.APPLICATION_JSON));

        try {

            return response.readEntity(NodeData.class);

        }catch (Exception e){

            String message = response.readEntity(String.class);
            throw new Exception(message + "***" + e);
        }

    }

    @Override
    public Integer getNodeStatus(int nodeId, SecurityToken token){

        Integer status = -1;
        String path = "node/status/" + String.valueOf(nodeId);

        try {

            status = client.target(REST_URL).path(path).request(MediaType.APPLICATION_JSON).
                    post(Entity.entity(token, MediaType.APPLICATION_JSON)).readEntity(Integer.class);

        }catch (Exception ex){

            LOGGER.error(ex);
        }

        return status;
    }

    @Override
    public boolean changeWorkFlag(int nodeId, int status, SecurityToken token){

        boolean result = false;
        String path = "node/flag/" + String.valueOf(nodeId) + "/" + String.valueOf(status);

        try {

            result = client.target(REST_URL).path(path).request(MediaType.APPLICATION_JSON).
                    post(Entity.entity(token, MediaType.APPLICATION_JSON)).readEntity(Boolean.class);

        }catch (Exception ex){

            LOGGER.error(ex);
        }

        return result;

    }

    @Override
    public Integer createNode(String nodeName, SecurityToken token){

        Integer result = -1;
        String path = "node/new/" + nodeName;

        try {

            result = client.target(REST_URL).path(path).request(MediaType.APPLICATION_JSON).
                    post(Entity.entity(token, MediaType.APPLICATION_JSON)).readEntity(Integer.class);

        }catch (Exception ex){

            LOGGER.error(" createNode " + ex);
        }

        return result;
    }

    @Override
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SIC_INNER_SHOULD_BE_STATIC_ANON", justification = "Exception detail hide")
    public List<UrlData> getTask(int nodeId, int amountTask, SecurityToken token) {

        String path = "task/get/" + String.valueOf(nodeId) + "/" + String.valueOf(amountTask);

        Response response = client.target(REST_URL).path(path).request(MediaType.APPLICATION_JSON).
                post(Entity.entity(token, MediaType.APPLICATION_JSON));

        return response.readEntity(new GenericType<List<UrlData>>() {
        });

    }

    @Override
    public boolean saveTaskResult(TaskResultDto taskResult) {

        return client.target(REST_URL).path("task/saveResult").request(MediaType.APPLICATION_JSON).
                post(Entity.entity(taskResult, MediaType.APPLICATION_JSON)).readEntity(Boolean.class);

    }

    @Override
    public boolean finalSaveInformation(FinalInformationDto finalInfo, int nodeId) {

        String path = "/task/finalSave/" + nodeId;


        return client.target(REST_URL).path(path).request(MediaType.APPLICATION_JSON).
                post(Entity.entity(finalInfo, MediaType.APPLICATION_JSON)).readEntity(Boolean.class);

    }

    @Override
    public void closeClient() {

        client.close();

    }

    @Override
    public void startClient() {

        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, 60000);
        configuration.property(ClientProperties.READ_TIMEOUT, 60000);

        client = ClientBuilder.newClient(configuration);

    }


}
