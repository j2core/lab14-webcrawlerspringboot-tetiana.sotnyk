package com.j2core.sts.webcrawler.taskservice.controller;

import com.j2core.sts.webcrawler.taskservice.model.informationdto.NodeData;
import com.j2core.sts.webcrawler.taskservice.model.userdto.SecurityToken;
import com.j2core.sts.webcrawler.taskservice.service.AuthenticationService;
import com.j2core.sts.webcrawler.taskservice.service.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The controller class for work with node's requests
 */
@Component
@Path("/node")
@Scope("request")
public class NodeController {

    private NodeService nodeService;
    private AuthenticationService authService;

    @Autowired
    public NodeController(NodeService nodeService, AuthenticationService authenticationService) {
        this.nodeService = nodeService;
        this.authService = authenticationService;
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = "/new/{nodeName}")
    public Integer createNode(@PathParam("nodeName") String nodeName, SecurityToken token){

        if (authService.equalsSecurityInfo(token)){

            return nodeService.createNewNode(nodeName);

        }else return -1;

    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path(value = "/flag/{nodeId}/{status : [0-1]}")
    public boolean changeWorkFlag(@PathParam("nodeId") int  nodeId, @PathParam("status") int status, SecurityToken token){

        boolean result = false;

        if (authService.equalsSecurityInfo(token)) {

            boolean workFlag;

            workFlag = status == 1;

            result =  nodeService.changeWorkNodeFlag(nodeId, workFlag);
        }
        return result;
    }


    @POST
    @Path(value = "/status/{nodeId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Integer getNodeStatus(@PathParam("nodeId") int nodeId, SecurityToken token){

        if (authService.equalsSecurityInfo(token)) {

            boolean result = nodeService.getNodeStatus(nodeId);
            if (result){
                return 1;
            }else {
                return 0;
            }
        }else return -1;
    }


    @POST
    @Path(value = "/get/{nodeId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public NodeData getNodeData(@PathParam("nodeId") int nodeId, SecurityToken token) {

        if (authService.equalsSecurityInfo(token)) {

            return nodeService.get(nodeId);

        } else {

            return null;
        }
    }
}
