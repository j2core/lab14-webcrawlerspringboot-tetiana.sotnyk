package com.j2core.sts.webcrawler.taskservice.service;

import com.j2core.sts.webcrawler.taskservice.dao.NodeDataDao;
import com.j2core.sts.webcrawler.taskservice.model.informationdto.NodeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 * Created by Sotnyk Tetiana.
 */
/**
 * The class is NodeService interface implementation
 */
@Service
public class DefaultNodeService implements NodeService {

    private NodeDataDao nodeDao;

    /**
     * Constructor
     *
     * @param nodeDataDao  object for work with node's data in DB
     */
    @Autowired
    public DefaultNodeService(NodeDataDao nodeDataDao) {

        this.nodeDao = nodeDataDao;
    }

    public int createNewNode(String nodeName) {

        NodeData nodeData = nodeDao.addNode(nodeName);

        return nodeData.getNodeId();

    }

    public boolean changeWorkNodeFlag(int nodeId, boolean status) {

        return nodeDao.setStatus(nodeId, status);

    }

    public boolean getNodeStatus(int nodeId) {

        NodeData nodeData = nodeDao.get(nodeId);

        return nodeData.isStatusWork();

    }

    public NodeData get(int nodeId){

        return nodeDao.get(nodeId);
    }

    public boolean changeNodeStatus(int nodeId){

        return nodeDao.setStatus(nodeId, false);

    }
}
