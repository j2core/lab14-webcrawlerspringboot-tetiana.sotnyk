package com.j2core.sts.webcrawler.taskservice.dao;

import com.j2core.sts.webcrawler.taskservice.model.userdto.RolesGroup;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The class is default implement interface RolesGroupDao
 */
@Repository
public class DefaultRolesGroupDao implements RolesGroupDao {

    private final static Logger LOGGER = Logger.getLogger(DefaultRolesGroupDao.class); // class for save logs information
    private EntityManager entityManager;


    @Autowired
    public DefaultRolesGroupDao(EntityManager entityManager) {

        this.entityManager = entityManager;
    }


    public List<RolesGroup> getAll(){

        List<RolesGroup> result = entityManager.createQuery("select e from RolesGroup e").getResultList();

        return result;
    }


    public RolesGroup get(int groupId) {

        return entityManager.find(RolesGroup.class, groupId);
    }


    public RolesGroup get(String groupName) {

        RolesGroup group;

        try {

            group = (RolesGroup) entityManager.createQuery("select e from RolesGroup e where e.groupName = :groupName").
                    setParameter("groupName", groupName).getSingleResult();

            return group;

        }catch (Exception ex){

            LOGGER.error(ex);
            return null;
        }

    }


    public boolean add(RolesGroup group) {

        try {

            entityManager.getTransaction().begin();
            entityManager.persist(group);
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

    }


    public boolean update(RolesGroup group) {

        try {

            entityManager.getTransaction().begin();
            entityManager.merge(group);
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);

        }

        return false;

    }


    public boolean delete(String groupName) {

        try {

            entityManager.getTransaction().begin();
            entityManager.createQuery("delete  from RolesGroup e where e.groupName = :groupName").
                    setParameter("groupName", groupName).executeUpdate();
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }
    }


    public boolean delete(int groupId) {

        try {

            entityManager.getTransaction().begin();
            entityManager.createQuery("delete  from RolesGroup e where e.groupId = :groupId").
                    setParameter("groupId", groupId).executeUpdate();
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }
    }
}
