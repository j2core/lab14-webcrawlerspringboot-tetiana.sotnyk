package com.j2core.sts.webcrawler.taskservice.dao;

import com.j2core.sts.webcrawler.taskservice.model.informationdto.PageInformation;
import com.j2core.sts.webcrawler.taskservice.model.informationdto.UrlData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The class is default implement interface PageInformationDao
 */
@Repository
public class DefaultPageInformationDao implements PageInformationDao {

    private final static Logger LOGGER = Logger.getLogger(DefaultPageInformationDao.class); // class for save logs information
    private EntityManager entityManager;

    @Autowired
    public DefaultPageInformationDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public int add(UrlData urlData, PageInformation pageInformation) {

        try {

            entityManager.createQuery("select e from PageInformation e where e.urlData.urlId = :urlData").
                    setParameter("urlData", urlData.getUrlId()).getSingleResult();

        } catch (NoResultException ex) {

            entityManager.clear();

            entityManager.getTransaction().begin();

            pageInformation = entityManager.merge(pageInformation);

            entityManager.getTransaction().commit();

            return pageInformation.getPageId();

        }

        return 0;
    }

    public PageInformation get(int pageId) {

        PageInformation result = null;

        try {

            result = entityManager.find(PageInformation.class, pageId);

        }catch (Exception ex){

            LOGGER.error(ex);

        }

        return result;
    }

    public PageInformation get(UrlData urlData) {

        PageInformation result = null;

        try {

            result = (PageInformation) entityManager.createQuery("select e from PageInformation e where e.urlData.urlId = :urlData").
                    setParameter("urlData", urlData.getUrlId()).getSingleResult();

        }catch (Exception ex){

            LOGGER.error(ex);

        }

        return result;
    }


    public boolean delete(int pageId) {

        try {

            entityManager.getTransaction().begin();
            entityManager.createQuery("delete  from PageInformation e where e.pageId = :pageId").
                    setParameter("pageId", pageId).executeUpdate();
            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

        return true;
    }

    public boolean delete(UrlData urlData) {

        try {

            entityManager.getTransaction().begin();
            entityManager.createQuery("delete  from PageInformation e where e.urlData.urlId = :urlData").
                    setParameter("urlData", urlData.getUrlId()).executeUpdate();
            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

        return true;
    }


    public boolean update(UrlData urlData, String pageText) {

        try {

            PageInformation pageInformation = (PageInformation) entityManager.createQuery("select e from PageInformation e where e.urlData.urlId = :urlData").
                    setParameter("urlData", urlData.getUrlId()).getSingleResult();

            pageInformation.setPageText(pageText);

            entityManager.getTransaction().begin();
            entityManager.merge(pageInformation);
            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

        return true;
    }
}
