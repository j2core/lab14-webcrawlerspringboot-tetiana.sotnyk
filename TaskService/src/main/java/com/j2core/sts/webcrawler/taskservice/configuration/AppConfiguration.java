package com.j2core.sts.webcrawler.taskservice.configuration;

import com.j2core.sts.webcrawler.taskservice.controller.AuthenticationController;
import com.j2core.sts.webcrawler.taskservice.controller.NodeController;
import com.j2core.sts.webcrawler.taskservice.controller.TaskController;
import com.j2core.sts.webcrawler.taskservice.dao.*;
import com.j2core.sts.webcrawler.taskservice.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Properties;
/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The application configuration class with beans
 */
public class AppConfiguration {

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        properties.put("hibernate.show_sql", "false");
        properties.put("hibernate.hbm2ddl.auto", "update");

        return properties;
    }

    @Bean
    @Scope("prototype")
    public javax.sql.DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/testCrawlerDB");
        dataSource.setUsername("sts");
        dataSource.setPassword("StsStsSts!2#");

        return dataSource;
    }

    @Bean
    @Scope("prototype")
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.j2core.sts.webcrawler.taskservice" });
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }

    @Bean
    @Scope("prototype")
    public EntityManager entityManager(){

        return Persistence.createEntityManagerFactory("crawler").createEntityManager();

    }

    @Bean
    @Scope("prototype")
    public NodeDataDao nodeDataDao(){

        return new DefaultNodeDataDao(entityManager());
    }

    @Bean
    @Scope("prototype")
    public NodeService nodeService(){

        return new DefaultNodeService(nodeDataDao());
    }

    @Bean
    public NodeController nodeController(){

        return new NodeController(nodeService(), authenticationService());
    }

    @Bean
    @Scope("prototype")
    public PermissionDao permissionDao(){

        return new DefaultPermissionDao(entityManager());
    }

    @Bean
    @Scope("prototype")
    public RolesGroupDao rolesGroupDao(){

        return new DefaultRolesGroupDao(entityManager());
    }

    @Bean
    @Scope("prototype")
    public UserDataDao userDataDao(){

        return new DefaultUserDataDao(entityManager());
    }

    @Bean
    @Scope("prototype")
    public AuthenticationService authenticationService(){

        return new DefaultAuthenticationService(userDataDao(), permissionDao());
    }

    @Bean
    public AuthenticationController authenticationController(){

        return new AuthenticationController(authenticationService());

    }

    @Bean
    @Scope("prototype")
    public PageInformationDao pageInformationDao(){

        return new DefaultPageInformationDao(entityManager());
    }

    @Bean
    @Scope("prototype")
    public UrlDataDao urlDataDao(){

        return new DefaultUrlDataDao(entityManager());
    }

    @Bean
    @Scope("prototype")
    public WordInformationDao wordInformationDao(){

        return new DefaultWordInformationDao(entityManager());
    }

    @Bean
    @Scope("prototype")
    public TaskService taskService(){

        return new DefaultTaskService(urlDataDao(), pageInformationDao(), wordInformationDao());
    }

    @Bean
    public TaskController taskController(){

        return new TaskController(taskService(), authenticationService(), nodeService());
    }

}
