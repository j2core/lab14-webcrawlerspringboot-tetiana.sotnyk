package com.j2core.sts.webcrawler.taskservice.controller;

import com.j2core.sts.webcrawler.taskservice.model.dto.FinalInformationDto;
import com.j2core.sts.webcrawler.taskservice.model.dto.TaskResultDto;
import com.j2core.sts.webcrawler.taskservice.model.informationdto.UrlData;
import com.j2core.sts.webcrawler.taskservice.model.userdto.SecurityToken;
import com.j2core.sts.webcrawler.taskservice.service.AuthenticationService;
import com.j2core.sts.webcrawler.taskservice.service.NodeService;
import com.j2core.sts.webcrawler.taskservice.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The controller class for work with task's requests
 */
@Component
@Path("/task")
@Scope("request")
public class TaskController {

    private TaskService taskService;
    private AuthenticationService authService;
    private NodeService nodeService;

    @Autowired
    public TaskController(TaskService taskService, AuthenticationService authService, NodeService nodeService) {
        this.taskService = taskService;
        this.authService = authService;
        this.nodeService = nodeService;
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = "/get/{nodeId}/{amountTask}")
    public List<UrlData> getTasks(@PathParam("nodeId") int  nodeId, @PathParam("amountTask") int amountTask, SecurityToken token){

        if (authService.equalsSecurityInfo(token)){

            List<UrlData> result = taskService.getNotProcessesTask(amountTask, nodeId);
            return result;

        }else return null;
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = "/saveResult")
    public boolean saveTaskResult(TaskResultDto resultDto) {


        if (authService.equalsSecurityInfo(resultDto.getToken())){

            return taskService.addTaskResult(resultDto.getResult());

        }else return false;

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = "/finalSave/{nodeId}")
    public boolean finalSaveTask(@PathParam("nodeId") int nodeId, FinalInformationDto finalDto) {

        boolean result = false;

        if (authService.equalsSecurityInfo(finalDto.getToken())) {
            result = taskService.finalSaveInformation(finalDto.getProcessedTask(), finalDto.getProcessesTask(),
                    finalDto.getNotProcessesTask(), nodeId);

        }



        return result;
    }


}
