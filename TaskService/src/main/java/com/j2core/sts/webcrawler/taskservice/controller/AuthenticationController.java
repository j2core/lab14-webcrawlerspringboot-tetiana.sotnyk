package com.j2core.sts.webcrawler.taskservice.controller;

import com.j2core.sts.webcrawler.taskservice.model.userdto.SecurityToken;
import com.j2core.sts.webcrawler.taskservice.model.userdto.UserData;
import com.j2core.sts.webcrawler.taskservice.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The controller class for work with authentication's requests
 */
@Component
@Path("/auth")
@Scope("request")
public class AuthenticationController {

    private AuthenticationService authService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService) {

        this.authService = authenticationService;
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = "/get")
    public SecurityToken get(UserData data){

        SecurityToken token;

        token = authService.getSecurityToken(data.getLogin(), data.getPassword());

        return token;
    }

}
