package com.j2core.sts.webcrawler.taskservice.dao;

import com.j2core.sts.webcrawler.taskservice.model.informationdto.NodeData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The class is default implement interface NodeDataDao
 */
@Repository
public class DefaultNodeDataDao implements NodeDataDao {

    private final static Logger LOGGER = Logger.getLogger(DefaultNodeDataDao.class); // class for save logs information

    private EntityManager entityManager;

    @Autowired
    public DefaultNodeDataDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public NodeData addNode(String nodeName) {

        NodeData nodeData = new NodeData(nodeName);

        try {

            entityManager.getTransaction().begin();
            entityManager.persist(nodeData);
            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return null;

        }

        entityManager.clear();
        return nodeData;
    }

    public NodeData get(int nodeId){

        NodeData nodeData;

        try {

            nodeData = entityManager.find(NodeData.class, nodeId);

        }catch (Exception ex){

            LOGGER.error(ex);
            return null;
        }

        entityManager.clear();
        return nodeData;
    }

    public boolean setStopFlag(int nodeId, boolean value) {

        NodeData nodeData;

        try {

            nodeData = entityManager.find(NodeData.class, nodeId);
            nodeData.setStopFlag(value);

            entityManager.getTransaction().begin();
            entityManager.merge(nodeData);
            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

        entityManager.clear();
        return true;
    }

    public boolean setStatus(int nodeId, boolean status) {

        NodeData nodeData;

        try {

            nodeData = entityManager.find(NodeData.class, nodeId);
            nodeData.setStatusWork(status);
            nodeData.setStopUnixTime(System.currentTimeMillis());

            entityManager.getTransaction().begin();
            entityManager.merge(nodeData);
            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

        entityManager.clear();
        return true;
    }

    public boolean getStopFlag(int nodeId) {

        NodeData nodeData = entityManager.find(NodeData.class, nodeId);

        entityManager.clear();
        return nodeData.isStopFlag();
    }

    public boolean getStatus(int nodeId) {

        NodeData nodeData = entityManager.find(NodeData.class, nodeId);

        entityManager.clear();
        return nodeData.isStatusWork();
    }
}
