package com.j2core.sts.webcrawler.taskservice.dao;

import com.j2core.sts.webcrawler.taskservice.model.informationdto.URLStatus;
import com.j2core.sts.webcrawler.taskservice.model.informationdto.UrlData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/*
 * Created by Sotnyk Tetiana.
 */

/**
 * The class is default implement interface UrlDataDao
 */
@Repository
public class DefaultUrlDataDao implements UrlDataDao {

    private final static Logger LOGGER = Logger.getLogger(DefaultUrlDataDao.class); // class for save logs information
    private EntityManager entityManager;

    @Autowired
    public DefaultUrlDataDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public UrlData get(int urlId) {

        UrlData result = null;

        try {

            result = entityManager.find(UrlData.class, urlId);

        }catch (Exception ex){

            LOGGER.error(ex);

        }

        return result;
    }


    public UrlData get(String url) {

        UrlData result = null;

        try {

            result = (UrlData) entityManager.createQuery("select e from UrlData e where e.url = :url").
                    setParameter("url", url).getSingleResult();

        }catch (Exception ex){

            LOGGER.error(ex);

        }

        return result;
    }


    public boolean updateStatus(int urlId, URLStatus status) {

        UrlData data = entityManager.find(UrlData.class, urlId);

        data.setStatus(status);
        data.setStatusChangeTime(System.currentTimeMillis());

        entityManager.getTransaction().begin();
        entityManager.merge(data);
        entityManager.getTransaction().commit();
        entityManager.clear();

        return true;
    }


    public boolean updateStatus(Collection<UrlData> collection, URLStatus status) {

        for (UrlData data : collection) {

            data.setStatusChangeTime(System.currentTimeMillis());
            data.setStatus(status);

            entityManager.getTransaction().begin();
            entityManager.merge(data);
            entityManager.getTransaction().commit();

        }

        entityManager.clear();
        return true;
    }


    public boolean update(UrlData urlData) {

        try {

            entityManager.getTransaction().begin();
            entityManager.merge(urlData);
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }
    }


    public boolean update(List<UrlData> collection) {

        try {
            entityManager.getTransaction().begin();

            for (UrlData urlData : collection){

                entityManager.merge(urlData);

            }
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;
        }
    }


    public boolean updateNodeId(int urlId, int nodeId) {

        throw new NotImplementedException();
    }


    public boolean updateNodeId(List<UrlData> urlDataList, int nodeId) {

        try {

            entityManager.getTransaction().begin();

            for (UrlData urlData: urlDataList){

                urlData.setNodeId(nodeId);
                entityManager.merge(urlData);
            }

            entityManager.getTransaction().commit();
            entityManager.clear();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;
        }

    }


    public boolean add(UrlData urlData) {

        try {

            try {

                entityManager.createQuery("select e from UrlData e where e.url = :url").
                        setParameter("url", urlData.getUrl()).getSingleResult();

            }catch (NoResultException ex){

                entityManager.clear();
                entityManager.getTransaction().begin();
                entityManager.persist(urlData);
                entityManager.getTransaction().commit();

            }

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;
        }

    }


    public boolean add(Collection<UrlData> urlDataCollection) {

        try {
            entityManager.getTransaction().begin();
            for (UrlData urlData : urlDataCollection) {

                try {

                    entityManager.createQuery("select e from UrlData e where e.url = :url").
                            setParameter("url", urlData.getUrl()).getSingleResult();

                } catch (NoResultException ex) {

                    entityManager.clear();

                    entityManager.persist(urlData);

                }

            }

            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;
        }

        return true;
    }


    public boolean delete(int urlId) {

        try {

            entityManager.getTransaction().begin();
            entityManager.createQuery("delete  from UrlData e where e.urlId = :urlId").
                    setParameter("urlId", urlId).executeUpdate();
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

    }


    public boolean delete(String url) {

        try {

            entityManager.getTransaction().begin();
            entityManager.createQuery("delete  from UrlData e where e.url = :url").
                    setParameter("url", url).executeUpdate();
            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

    }


    public List<UrlData> getNotProcessesUrl(int amount, int nodeId){

        List<UrlData> result = entityManager.createQuery("select e from UrlData e where e.status = :status and e.amountReadPage < 7").
                setParameter("status", URLStatus.NOT_PROCESSED).setMaxResults(amount).getResultList();
        entityManager.clear();

        if (result.isEmpty()) return new LinkedList<UrlData>();
        if (updateNodeId(result, nodeId) && updateStatus( result, URLStatus.PROCESSES) && updateAmountReadPage(result) ) {

            return result;

        } else return new LinkedList<UrlData>();

    }


    public List<UrlData> getDeprecateUrl(int amount, int deprecateTime, int nodeId) {

        long unixTime = System.currentTimeMillis();
        long oldTime = unixTime - deprecateTime;
        List<UrlData> result = entityManager.createQuery("select e from UrlData e where e.status = :status " +
                "and e.statusChangeUnixTime < :time and e.nodeId != :nodeId and e.amountReadPage < 7").setParameter("status", URLStatus.PROCESSES)
                .setParameter("time", oldTime).setParameter("nodeId", nodeId).setMaxResults(amount).getResultList();

        if (result.isEmpty()){
            return new LinkedList<UrlData>();
        }else{
            entityManager.clear();

            if (updateNodeId(result, nodeId) && updateStatus( result, URLStatus.PROCESSES)) {

                return result;

            } else
                return new LinkedList<UrlData>();
        }

    }

    public boolean updateAmountReadPage(int urlId) {

        UrlData result;
        try {

            result = entityManager.find(UrlData.class, urlId);

            result.setAmountReadPage(result.getAmountTransition() + 1);

            entityManager.getTransaction().begin();

            entityManager.merge(result);

            entityManager.getTransaction().commit();

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }
    }

    public int getAmountReadPage(int urlId) {

        UrlData result = null;

        try {

            result = entityManager.find(UrlData.class, urlId);

        }catch (Exception ex){

            LOGGER.error(ex);

        }

        assert result != null;
        return result.getAmountReadPage();
    }

    public boolean updateAmountReadPage(List<UrlData> urlDataList) {

        try {

            for (UrlData url : urlDataList){

                updateAmountReadPage(url.getNodeId());

            }

            return true;

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

    }

}
